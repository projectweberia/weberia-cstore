![CStore](https://github.com/weberia/assets/raw/master/images/logo-cstore.png "CStore logo")

CStore is a social commitment-based data store for pragmatic web systems. It is implemented as microservice, developed mainly using [Colossus](http://tumblr.github.io/colossus/)

# License

Copyright © 2015 [Bambang Purnomosidi D. P.](http://bpdp.name)

Distributed under the [Apache License - version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)
