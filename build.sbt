organization := "Project Weberia"

name := "CStore"

version := "1.0.0"

scalaVersion := "2.11.7"

resolvers ++= Seq(
  "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/",
  "Sonatype OSS Releases" at "https://oss.sonatype.org/content/repositories/releases",
  "apache-repo-releases" at "http://repository.apache.org/content/repositories/releases/",
  "Scalaz Bintray Repo" at "http://dl.bintray.com/scalaz/releases"
)

libraryDependencies ++= {

  val (akkaV, streamHttp) = ("2.3.11", "1.0-RC4")

  // remember, %% means scala-major-version, so %% "libname" will 
  // result error if no libname_2.11 available. Use % instead
  // see: http://www.scala-sbt.org/0.13/tutorial/Library-Dependencies.html

  Seq(
    "com.tumblr"  %%  "colossus"    % "0.6.3",
    "org.specs2"  %%  "specs2-core" % "3.6.2"   % "test"
  )

}

scalacOptions ++= Seq("-unchecked", "-deprecation", "-feature")

// sbt related setting
//shellPrompt := { state => "[" + System.getProperty("user.name") + "][" + Project.extract(state).currentRef.project + "] " }
